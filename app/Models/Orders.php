<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    protected $table = 'orders';
    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo(Pizzas::class);
    }
}
