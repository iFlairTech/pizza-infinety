<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pizzas extends Model
{
    //
    protected $table = 'pizzas';
    protected $guarded = ['id'];

    /**
    * Get all orders for the user
    */
    public function pizzas()
    {
        return $this->hasMany(Orders::class);
    }
}
