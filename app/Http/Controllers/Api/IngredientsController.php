<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Ingredients;
use \App\Models\Pizzas;
use \App\Models\Orders;
use Response;

class IngredientsController extends Controller
{
    public function index(Request $request)
    {
        
        $data= Ingredients::where('status','0')->get();
        if(count($data)>0){
        	return Response::json(['status'=>'success','data'=>$data]);
        }
    }


    public function store(Request $request){
    	if($request->ingredient_name==''){
			return Response::json(['status'=>'error','msg'=>'Ingredient name is required']);
    	}
    	else{
    		$store= Ingredients::create(['ingredient_name'=>$request->ingredient_name]);
    		if($store){
    			return Response::json(['status'=>'success','msg'=>'Ingredient created successfully']);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
    		}
    	}
    }

    public function edit(Request $request,$id){
    	
    	if($id){
    		$data= Ingredients::where('id',$id)->first();
    		if($data){
    			return Response::json(['status'=>'success','data'=>$data]);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'No record found for matching id']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    	}
    }

    public function view(Request $request,$id){
        
        if($id){
            $data= Ingredients::where('id',$id)->first();
            if($data){
                return Response::json(['status'=>'success','data'=>$data]);
            }
            else{
                return Response::json(['status'=>'error','msg'=>'No record found for matching id']);
            }
        }
        else{
            return Response::json(['status'=>'error','msg'=>'Something went wrong']);
        }
    }


    public function update(Request $request,$id){
    	if($request->ingredient_name==''){
			return Response::json(['status'=>'error','msg'=>'Ingredient name is required']);
    	}
    	else{
    		if($id){
	    		$update= Ingredients::where('id',$id)->update(['ingredient_name'=>$request->ingredient_name]);
	    		if($update){
	    			return Response::json(['status'=>'success','msg'=>'Ingredient updated successfully']);
	    		}
	    		else{
	    			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
	    		}
	    	}
	    	else{
				return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
	    	}
    	}
    }

    public function delete(Request $request,$id){
    	
    	if($id){
    		$data=  Ingredients::where('id',$id)->update(['status'=>'2']);
                    Pizzas::whereRaw("find_in_set('".$id."',ingredients)")->update(['status'=>'2']);
                    Orders::whereRaw("find_in_set('".$id."',ingredients)")->update(['status'=>'2']);
    		if($data){
    			return Response::json(['status'=>'success','msg'=>'Ingredient deleted successfully']);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    	}
    }

    public function countdata(){
        $ingredients= Ingredients::where('status','0')->get()->count();
        $orders= Orders::where('status','0')->get()->count();
        $pizzas= Pizzas::where('status','0')->get()->count();
        return Response::json(['status'=>'success','ingredients'=>$ingredients,'orders'=>$orders,'pizzas'=>$pizzas]);
    }
}
