<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Orders;
use \App\Models\Pizzas;
use \App\Models\Ingredients;
use Response;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        
        $data= Orders::where('status','0')->get();
        foreach ($data as $key => $value) {
            $array=[];
            $arr = explode(',', $value->ingredients);
            foreach ($arr as $k => $val) {
                $array1=[];
                $ing= Ingredients::where('id',$val)->first();
                if(!empty($ing)){
                    $array1['id']=$val;
                    $array1['name']=$ing->ingredient_name;
                    $array[$k]=$array1;
                }

            }
            $data[$key]->ing_array= $array;
        }
        if(count($data)>0){
        	return Response::json(['status'=>'success','data'=>$data]);
        }
    }


    public function store(Request $request){
    	// $ingredients='';
     //    if(isset($request->ingredients) && !empty($request->ingredients)){
     //        $ingredients = implode(',', $request->ingredients);
     //    }
        $pizza_id= $request->pizza_id;
        $customer_email= $request->customer_email;
        $pizzadetails= Pizzas::where('id',$pizza_id)->first();  
		$store= Orders::create(['pizza_name'=>$pizzadetails->pizza_name,'customer_email'=>$customer_email,'price'=>$pizzadetails->price,'ingredients'=>$pizzadetails->ingredients,'pizza_id'=>$pizza_id]);
		if($store){
			return Response::json(['status'=>'success','msg'=>'Order created successfully']);
		}
		else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
		}
    	
    }

    public function edit(Request $request,$id){
    	
    	if($id){
    		$data= Orders::where('id',$id)->first();
    		if($data){
    			return Response::json(['status'=>'success','data'=>$data]);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'No record found for matching id']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    	}
    }
    
    public function view(Request $request,$id){
        
        if($id){
            $data= Orders::where('id',$id)->first();
            if($data){
                $array=[];
                if($data->ingredients!=''){
                    $arr = explode(',', $data->ingredients);
                    foreach ($arr as $k => $val) {
                        $array1=[];
                        $ing= Ingredients::where('id',$val)->first();
                      
                        $array1['id']=$val;
                        $array1['name']=$ing->ingredient_name;
                        $array[$k]=$array1;

                    }
                }
                $data->ingredient_details= $array;
                return Response::json(['status'=>'success','data'=>$data]);
            }
            else{
                return Response::json(['status'=>'error','msg'=>'No record found for matching id']);
            }
        }
        else{
            return Response::json(['status'=>'error','msg'=>'Something went wrong']);
        }
    }
    public function update(Request $request,$id){
    	if($id){
            $updatedata= ['pizza_name'=>$request->pizza_name,'customer_email'=>$request->customer_email,'price'=>$request->price];
            if(isset($request->ingredients) && !empty($request->ingredients)){
                $ingredients = implode(',', $request->ingredients);
                $updatedata['ingredients']= $ingredients;
            }
    		$update= Orders::where('id',$id)->update($updatedata);
    		if($update){
    			return Response::json(['status'=>'success','msg'=>'Order updated successfully']);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
    	}
    	
    }

    public function delete(Request $request,$id){
    	
    	if($id){
    		$data= Orders::where('id',$id)->update(['status'=>'2']);
    		if($data){
    			return Response::json(['status'=>'success','msg'=>'Order deleted successfully']);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    	}
    }

}
