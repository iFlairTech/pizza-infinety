<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Models\Pizzas;
use \App\Models\Ingredients;
use Illuminate\Support\Facades\Storage;
use Response;

class PizzasController extends Controller
{
    public function index(Request $request)
    {
        
        $data= Pizzas::where('status','0')->get();
        foreach ($data as $key => $value) {
            $array=[];
            $arr = explode(',', $value->ingredients);
            foreach ($arr as $k => $val) {
                $array1=[];
                $ing= Ingredients::where('id',$val)->first();
                if(!empty($ing)){
                    $array1['id']=$val;
                    $array1['name']=$ing->ingredient_name;
                    $array[$k]=$array1;
                }

            }
            $data[$key]->ing_array= $array;
        }
        if(count($data)>0){
        	return Response::json(['status'=>'success','data'=>$data]);
        }
    }


    public function store(Request $request){
    	$ingredients='';
        if(isset($request->ingredients) && !empty($request->ingredients)){
            $ingredients = implode(',', $request->ingredients);
        }
                        
        $imagenm='';
        if (!empty($request->file('image')))
        {
            // Image name
            $requestImage = $request->file('image');
            $result_image = preg_replace('/ /','-',$requestImage->getClientOriginalName());
            $imagenm = time().'_'.$result_image;
            $request->image->storeAs('images/pizzas',$imagenm,'public');
            
        }
		$store= Pizzas::create(['pizza_name'=>$request->pizza_name,'price'=>$request->price,'ingredients'=>$ingredients,'image'=>$imagenm]);
		if($store){
			return Response::json(['status'=>'success','msg'=>'Record created successfully']);
		}
		else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
		}
    	
    }

    public function edit(Request $request,$id){
    	
    	if($id){
    		$data= Pizzas::where('id',$id)->first();
    		if($data){
    			return Response::json(['status'=>'success','data'=>$data]);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'No record found for matching id']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    	}
    }

    public function view(Request $request,$id){
        
        if($id){
            $data= Pizzas::where('id',$id)->first();
            if($data){
                $array=[];
                if($data->ingredients!=''){
                    $arr = explode(',', $data->ingredients);
                    foreach ($arr as $k => $val) {
                        $array1=[];
                        $ing= Ingredients::where('id',$val)->first();
                      
                        $array1['id']=$val;
                        $array1['name']=$ing->ingredient_name;
                        $array[$k]=$array1;

                    }
                }
                $data->ingredient_details= $array;
                return Response::json(['status'=>'success','data'=>$data]);
            }
            else{
                return Response::json(['status'=>'error','msg'=>'No record found for matching id']);
            }
        }
        else{
            return Response::json(['status'=>'error','msg'=>'Something went wrong']);
        }
    }

    public function update(Request $request,$id){
    	if($id){
            $updatedata= ['pizza_name'=>$request->pizza_name,'price'=>$request->price];

            if(isset($request->ingredients) && !empty($request->ingredients)){
                $ingredients = implode(',', $request->ingredients);
                $updatedata['ingredients']= $ingredients;
            }

            if (!empty($request->file('image')))
            {
                $oldPizza = Pizzas::find($id);
                if($oldPizza->image){
                    $del=Storage::disk('public')->delete("images/pizzas/" . $oldPizza->image);
                    $filename = preg_replace('/ /','-',$request->image->getClientOriginalName());
                    $imagenm = time().'_'.$filename;
                    $request->image->storeAs('images/pizzas',$imagenm,'public');
                }
                
                $updatedata['image']=$imagenm;
                
            }
    		$update= Pizzas::where('id',$id)->update($updatedata);
    		if($update){
    			return Response::json(['status'=>'success','msg'=>'Record updated successfully']);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong!']);
    	}
    	
    }

    public function delete(Request $request,$id){
    	
    	/*if($id){
    		$data= Pizzas::where('id',$id)->update(['status'=>'2']);
    		if($data){
    			return Response::json(['status'=>'success','msg'=>'Record deleted successfully']);
    		}
    		else{
    			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    		}
    	}
    	else{
			return Response::json(['status'=>'error','msg'=>'Something went wrong']);
    	}*/

        $pizza = Pizzas::find($id);

        if (!$pizza) {
            return response()->json(['status'=>'error','msg'=>'Something went wrong']);
        }

        $pizza->destroy($id);

        return response()->json(['status'=>'success','msg'=>'Record deleted successfully']);
    }

}
