<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');


Route::group(['middleware' => 'jwt.auth'], function () {

	Route::get('ingredients', 'Api\IngredientsController@index');
	Route::post('ingredients/store', 'Api\IngredientsController@store');
	Route::post('ingredients/edit/{id?}', 'Api\IngredientsController@edit');
	Route::get('ingredients/view/{id?}', 'Api\IngredientsController@view');
	Route::post('ingredients/update/{id?}', 'Api\IngredientsController@update');
	Route::post('ingredients/delete/{id?}', 'Api\IngredientsController@delete');
	Route::get('countdata', 'Api\IngredientsController@countdata');


	Route::get('pizzas', 'Api\PizzasController@index');
	Route::post('pizzas/store', 'Api\PizzasController@store');
	Route::post('pizzas/edit/{id?}', 'Api\PizzasController@edit');
	Route::get('pizzas/view/{id?}', 'Api\PizzasController@view');
	Route::post('pizzas/update/{id?}', 'Api\PizzasController@update');
	Route::post('pizzas/delete/{id?}', 'Api\PizzasController@delete');


	Route::get('orders', 'Api\OrdersController@index');
	Route::post('orders/store', 'Api\OrdersController@store');
	Route::post('orders/edit/{id?}', 'Api\OrdersController@edit');
	Route::get('orders/view/{id?}', 'Api\OrdersController@view');
	Route::post('orders/update/{id?}', 'Api\OrdersController@update');
	Route::post('orders/delete/{id?}', 'Api\OrdersController@delete');
});

