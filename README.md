# Pizza infinety backend
A Simple Pizza Item Create/Read/Update/Delete (CRUD) Application created using Laravel 7.29 and Vue.js 2.6.12 with composer and npm package or dependency management.

## Requirements
    - PHP 7.1+
    - Composer
    - Node
    - NPM

To work on the project:
    - **Make your proper module branch.**

## Setup Project  

After cloning on your machine:

    - Open the console and cd your project root directory
    - Run `composer install` or `php composer.phar install`
    - Run `php artisan key:generate`
    - Run `php artisan migrate`
    - Run `php artisan db:seed` to run seeders, if any.
    - Run `php artisan serve`
    - Install all packages `npm install`
    - Setup Environments by copying file `.env.example` to `.env`
    - Configure the database variables, etc.
    - set in .env for image store: MIX_VUE_APP_IMGURL=/storage/images/pizzas/

*Please do not commit and push your configuration files into the repository!*

Run project in

    - Development Mode 
    
        - `npm run watch` & `php artisan serve`
