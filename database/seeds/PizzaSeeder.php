<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pizzas')->insert([
       		['pizza_name' => 'Cheese burst','price'=>300,'image'=>'abc.png','ingredients'=>'1,2'],
       		['pizza_name' => 'Margerita','price'=>400,'image'=>'test.png','ingredients'=>'1'],
       		['pizza_name' => 'Farmhouse','price'=>500,'image'=>'test1.png','ingredients'=>'1,2'],
       	]);
    }
}
