<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       	DB::table('orders')->insert([
       		['pizza_name' => 'Cheese burst','customer_email'=>'test1@gmail.com','price'=>300,'ingredients'=>'1,2','pizza_id'=>'1'],
       		['pizza_name' => 'Margerita','customer_email'=>'test2@gmail.com','price'=>400,'ingredients'=>'1','pizza_id'=>'2'],
       		['pizza_name' => 'Farmhouse','customer_email'=>'test3@gmail.com','price'=>500,'ingredients'=>'1,2','pizza_id'=>'3'],
       	]);
    }
}
