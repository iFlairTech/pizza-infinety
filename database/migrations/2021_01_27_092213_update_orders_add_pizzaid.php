<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersAddPizzaid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            // $table->unsignedBigInteger('pizza_id')->after('status')->nullable();
            $table->bigInteger('pizza_id')->after('status')->unsigned()->nullable();
            $table->enum('delivery_status', ['pending','preparing','cancelled','completed'])->nullable()->default('pending')->after('status');
            // $table->bigInteger('pizza_id')->unsigned()->nullable()->change();
            $table->foreign('pizza_id')->references('id')->on('pizzas')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
