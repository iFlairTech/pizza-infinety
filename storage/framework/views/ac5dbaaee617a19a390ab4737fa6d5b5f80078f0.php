

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <!-- <div class="col-md-2">
            <menu-sidebar></menu-sidebar>
        </div> -->
        <transition name="fade" mode="out-in" appear>
            <router-view></router-view>
        </transition>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\pizza-infinety\resources\views/home.blade.php ENDPATH**/ ?>