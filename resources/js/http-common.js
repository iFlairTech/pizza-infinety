import axios from "axios";
let apiClient= axios.create({
  baseURL: process.env.MIX_VUE_APP_APIURL,
  headers: {
    "Content-type": "application/json"  }
});

apiClient.interceptors.request.use(function (config) {
  const token = localStorage.getItem('token')

  if(config.url != '/login') {
    // let url = config.url
    // url = url+'?token='+token
    // config.url = url;
    config.headers["Authorization"] = `Bearer ${token}`
  }

  return config;
});

export default apiClient
