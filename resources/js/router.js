import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/dashboard",
            name: "dashboard",
            component: () => import("./components/dashboard/dashboard")
        },
        {
            
            path: "/home",
            name: "home",
            component: () => import("./components/home/home")
        },
        {
            path: "/",
            alias: "/login",
            name: "login",
            component: () => import("./components/login/login")
        },
        {
            path: "/ingredients",
            name: "ingredients",
            component: () => import("./components/ingredients/ingredientsList")
        },
        {
            path: "/add-ingredients",
            name: "add-ingredients",
            component: () => import("./components/ingredients/addIngredients")
        },
        {
            path: "/add-ingredients/:id",
            name: "edit-ingredients",
            component: () => import("./components/ingredients/addIngredients")
        },
        {
            path: "/pizzas",
            name: "pizzas",
            component: () => import("./components/pizzas/pizzasList")
        },
        {
            path: "/add-pizza/:id",
            name: "edit-pizza",
            component: () => import("./components/pizzas/addPizza")
        },
        {
            path: "/add-pizza",
            name: "add-pizza",
            component: () => import("./components/pizzas/addPizza")
        }
    ]
});
