import http from "../http-common";
import Vue from 'vue';
import { Subject,Observable, BehaviorSubject } from 'rxjs';

let checkLogin = new BehaviorSubject();


class commonDataservice {

  //Ingredients APIs
  getAllIngredients() {
    return http.get(`/ingredients`);
  }
  createIngredients(data) {
    return http.post(`/ingredients/store`, data)
  }
  deleteIngredients(id) {
    return http.post(`/ingredients/delete/${id}`)
  }
  getIngredientById(id) {
    return http.get(`/ingredients/view/${id}`);
  }
  editIngredients(data, id) {
    return http.post(`/ingredients/update/${id}`, data)
  }

  //PIZZA APIs
  getAllPizza() {
    return http.get(`/pizzas`);
  }
  createPizza(data) {
    return http.post(`/pizzas/store`, data)
  }
  deletePizza(id) {
    return http.post(`/pizzas/delete/${id}`)
  }
  getPizzaById(id) {
    return http.get(`/pizzas/view/${id}`);
  }
  editPizza(data, id) {
    return http.post(`/pizzas/update/${id}`, data)
  }
  countItems() {
    return http.get(`/countdata`)
  }

  //order api
  getAllOrders() {
    return http.get(`/orders`);
  }
  createOrder(data) {
    return http.post(`/orders/store`, data)
  }

  //login
  login(data){
    return http.post(`/login`,data)
  }
  // for display toast
  displayToast(type, msg) {
    Vue.$toast.open({
      type: type,
      message: msg,
      position: "top-right"
    })
  }

  setStatus(data) {
    checkLogin.next(data);
  }

  getStatus() {
    return checkLogin.asObservable();
  }

  // isLoggedIn(){
  //   if(localStorage.getItem('token')){
  //     checkLogin.next(true)
  //     // return true;
  //   }
  //   else{
  //     checkLogin.next(false)
  //     // return false;
  //   }
  // }

}

export default new commonDataservice();