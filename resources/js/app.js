require('./bootstrap');

import Vue from 'vue';
import App from './components/App.vue';
import vuetify from './plugins/vuetify';
import router from './router';
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'
// import './assets/global.css'
import VueToast from 'vue-toast-notification';
// import 'vue-toast-notification/dist/theme-sugar.css';
import Vuelidate from 'vuelidate';



Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(VueToast);

// Vue.component('app', require('./components/App.vue').default);
Vue.component('addPizza', require('./components/pizzas/addPizza.vue').default);
Vue.component('pizzasList', require('./components/pizzas/pizzasList.vue').default);
Vue.component('login', require('./components/login/login.vue').default);
Vue.component('ingredientsList', require('./components/ingredients/ingredientsList.vue').default);
Vue.component('addIngredients', require('./components/ingredients/addIngredients.vue').default);
Vue.component('home', require('./components/home/home.vue').default);
Vue.component('dashboard', require('./components/dashboard/dashboard.vue').default);


new Vue({
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app')
